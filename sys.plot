MAXMEM=4096 # MB
set term png medium size 1000,500
set output "sys.png"
set xlabel "seconds"
set ylabel "percent CPU usage"
set y2label "memory (MB)"
set y2tics
set yrange [0:400]
set y2range [0: MAXMEM]
set y2tics nomirror
set ytics nomirror
set style fill transparent solid 0.2 noborder
plot \
	"sys.dat" using 2 with filledcurves above x1 axis x1y1 lt rgb "red" title "CPU 1", \
	"sys.dat" using 3 with filledcurves above x1 axis x1y1 lt rgb "blue" title "CPU 2", \
	"sys.dat" using 4 with filledcurves above x1 axis x1y1 lt rgb "green" title "CPU 3", \
	"sys.dat" using 5 with filledcurves above x1 axis x1y1 lt rgb "yellow" title "CPU 4", \
	"sys.dat" using 6 with lines axis x1y1 lt rgb "#25ff25" title "User", \
	"sys.dat" using 7 with lines axis x1y1 lt rgb "#ffc425" title "System", \
	"sys.dat" using 8 with lines axis x1y1 lt rgb "#ff0000" title "wait I/O", \
	"sys.dat" using 9 with lines axis x1y2 lt rgb "#2525ff" title "avail mem"

