# cpumemgraph

A simple tool that makes graphs of CPU and memory usage, ideal to monitor the performance of a program and its effect on the OS.

It uses [top](https://gitlab.com/procps-ng/procps/-/tree/master/top) tool to gather CPU and memory
data, [GNU Awk](https://www.gnu.org/software/gawk/) as a scripting language that compiles data in a
plottable way, and [Gnuplot](http://www.gnuplot.info/) for plotting. It is used with GNU/Linux, but
may work on similar systems.

It provides statistics on available memory in the system, CPU core usage and total percent of time
spent in user/kernel/wait. The top tool gives these measurement every second. The number of cores
and type of data captured need to be adjusted from the awk script.

![example graph](https://gitlab.com/free-astro/cpumemgraph/-/blob/master/sys.png?inline=false "example graph")

## Usage

This is the capture command:

```shell
$ while [[ true ]] ; do top -b1 -n1 -d1 -Em -p0 | awk -f log.awk ; sleep 1; done > sys.dat
```

This calls top every second, formats its data into a line that can be used for plotting in the
sys.dat file. Once the capture needs to be processed, this is the gnuplot command:

```shell
$ gnuplot sys.plot
```

